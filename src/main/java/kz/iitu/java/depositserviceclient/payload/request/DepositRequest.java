package kz.iitu.java.depositserviceclient.payload.request;

import lombok.Data;

@Data
public class DepositRequest {
    private int maxReplenishment;
    private int limitationTime;
}
