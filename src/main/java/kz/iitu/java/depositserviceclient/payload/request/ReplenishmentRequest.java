package kz.iitu.java.depositserviceclient.payload.request;

import lombok.Data;

@Data
public class ReplenishmentRequest {
    private int replenishment;
    private String clientId;
}
