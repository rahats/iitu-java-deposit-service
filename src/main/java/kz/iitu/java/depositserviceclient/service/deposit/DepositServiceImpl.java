package kz.iitu.java.depositserviceclient.service.deposit;

import kz.iitu.java.depositserviceclient.domain.Deposit;
import kz.iitu.java.depositserviceclient.exception.CustomConflictException;
import kz.iitu.java.depositserviceclient.exception.CustomNotFoundException;
import kz.iitu.java.depositserviceclient.payload.request.DepositRequest;
import kz.iitu.java.depositserviceclient.payload.request.ReplenishmentRequest;
import kz.iitu.java.depositserviceclient.repository.DepositRepository;
import kz.iitu.java.depositserviceclient.service.validation.ValidationService;
import kz.iitu.java.depositserviceclient.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service @RequiredArgsConstructor
public class DepositServiceImpl implements DepositService{

    private final DepositRepository repository;

    @Override
    public ResponseEntity<?> openDeposit(String clientId, DepositRequest request) {
        ValidationService.requestDepositValidation(request);
        Deposit deposit = Deposit.builder()
                .maxReplenishment(request.getMaxReplenishment())
                .limitationTime(request.getLimitationTime())
                .clientId(clientId)
                .build();
        save(deposit);
        return ResponseUtil.response("Депозит открыт");
    }

    @Override
    public void save(Deposit deposit) {
        repository.save(deposit);
    }

    @Override
    public ResponseEntity<?> replenishmentDeposit(ReplenishmentRequest request) {
        Deposit deposit = findByClientId(request.getClientId());
        ValidationService.replenishmentDepositValidation(request , deposit.getMaxReplenishment());
        deposit.setCurrentBalance(request.getReplenishment());
        save(deposit);
        return ResponseUtil.response("Депозит пополнен");
    }

    @Override
    public Deposit findByClientId(String clientId) {
        return repository.findByClientId(clientId).orElseThrow(()-> new CustomNotFoundException("Депозит не найден"));
    }

    @Override
    public int cashWithdrawal(String clientId, int cash) {
        Deposit deposit = findByClientId(clientId);
        if (deposit.getCurrentBalance()<cash)
            throw new CustomConflictException("Запрошенная сумма больше текущего баланса");
        if (deposit.getCurrentBalance()-cash<1000)
            throw new CustomConflictException("Минимальный баланс не может быть менешь 1000 тг");
        deposit.setCurrentBalance(deposit.getCurrentBalance()-cash);
        save(deposit);
        return cash;
    }

    @Override
    public int closeDeposit(String clientId) {
        Deposit deposit = findByClientId(clientId);
        int remainder = deposit.getCurrentBalance();
        repository.delete(deposit);
        return remainder;
    }
}
