package kz.iitu.java.depositserviceclient.service.deposit;

import kz.iitu.java.depositserviceclient.domain.Deposit;
import kz.iitu.java.depositserviceclient.payload.request.DepositRequest;
import kz.iitu.java.depositserviceclient.payload.request.ReplenishmentRequest;
import org.springframework.http.ResponseEntity;

public interface DepositService {
    ResponseEntity<?> openDeposit(String clientId , DepositRequest request);
    void save(Deposit deposit);
    ResponseEntity<?> replenishmentDeposit(ReplenishmentRequest request);
    Deposit findByClientId(String clientId);
    int cashWithdrawal(String clientId , int cash);
    int closeDeposit(String clientId);
}
