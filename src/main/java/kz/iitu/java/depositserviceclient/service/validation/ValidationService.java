package kz.iitu.java.depositserviceclient.service.validation;

import kz.iitu.java.depositserviceclient.exception.CustomConflictException;
import kz.iitu.java.depositserviceclient.payload.request.DepositRequest;
import kz.iitu.java.depositserviceclient.payload.request.ReplenishmentRequest;

public class ValidationService {

    public static void requestDepositValidation(DepositRequest request) {
        replenishmentAvailability(request.getMaxReplenishment());
        limitationTimeAvailability(request.getLimitationTime());
    }

    public static void replenishmentDepositValidation(ReplenishmentRequest request, int maxReplenishment) {
        replenishmentAvailability(request.getReplenishment());
        if (request.getReplenishment() > maxReplenishment)
            throw new CustomConflictException(String.format("Макс. сумма депозита %s . Вы можете поменять макс. сумму",
                    maxReplenishment));

    }

    private static void replenishmentAvailability(int replenishment) {
        if (replenishment < 1000)
            throw new CustomConflictException("Сумма депозита не должна быть меньше 1000 тг");
    }

    private static void limitationTimeAvailability(int limitationTime) {
        if (limitationTime != 12)
            throw new CustomConflictException("Срок депозита только год");
    }
}
