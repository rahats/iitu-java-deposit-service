package kz.iitu.java.depositserviceclient.repository;

import kz.iitu.java.depositserviceclient.domain.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DepositRepository extends JpaRepository<Deposit , Long> {
    Optional<Deposit> findByClientId(String id);
}
