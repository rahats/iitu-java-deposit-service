package kz.iitu.java.depositserviceclient.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import kz.iitu.java.depositserviceclient.exception.CustomConflictException;
import kz.iitu.java.depositserviceclient.payload.request.DepositRequest;
import kz.iitu.java.depositserviceclient.payload.request.ReplenishmentRequest;
import kz.iitu.java.depositserviceclient.service.deposit.DepositService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/deposit")
@RequiredArgsConstructor
public class DepositController {

    private final DepositService service;

    @HystrixCommand(fallbackMethod = "mockAnswer")
    @PostMapping(value = "/open/{clientId}")
    public ResponseEntity<?> openDeposit(@PathVariable String clientId , @RequestBody DepositRequest request){
        return service.openDeposit(clientId, request);
    }

    @PostMapping(value = "/replenishment")
    public ResponseEntity<?> replenishmentDeposit(@RequestBody ReplenishmentRequest request){
        return service.replenishmentDeposit(request);
    }

    @GetMapping(value = "/withdrawal/{clientId}/{cash}")
    public int cashWithdrawal(@PathVariable String clientId , @PathVariable int cash){
        return service.cashWithdrawal(clientId, cash);
    }

    @GetMapping(value = "/close/{clientId}")
    public int closeDeposit(@PathVariable String clientId){
        return service.closeDeposit(clientId);
    }

    private ResponseEntity<?> mockAnswer(String clientId , DepositRequest request) {
        throw new CustomConflictException("Сервис временно не работает");
    }
}
